from django.conf.urls import url

from main.views import *

urlpatterns = [
    url(r'^$', mainView, name='index'),
    url('features/', featuresView, name='features')
]
