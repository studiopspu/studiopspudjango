from datetime import datetime
from threading import Thread

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
import loginApp
from calendarApp.ProcessEmail import sendEmail
from calendarApp.models import time
from structs import buildCalendar, buildHeaderNavItems, linkType


def redirectIfNeed(request):
    current_user = request.user
    if not current_user.is_authenticated:
        response = redirect('/auth/login/?next=/calendar')
        return response


# TODO replace with decorator

def calendar(request):
    ret = redirectIfNeed(request)
    if ret is not None:
        return ret
    today = datetime.now()
    return render(request, 'calendar.html', {"calendar": buildCalendar(request, int(today.year), int(today.month)), 'headerNav': buildHeaderNavItems(['Главная страница', 'Инструкции', 'Таблица записи'],
                                                                           ['index', 'features', 'calendar'],
                                                                           [linkType.pattern, linkType.pattern, linkType.pattern])})


def calendarParametrized(request, year, month):
    ret = redirectIfNeed(request)
    if ret != None:
        return ret
    return render(request, 'calendar.html', {"calendar": buildCalendar(request, int(year), int(month)), 'headerNav': buildHeaderNavItems(['Главная страница', 'Инструкции', 'Таблица записи'],
                                                                           ['index', 'features', 'calendar'],
                                                                           [linkType.pattern, linkType.pattern, linkType.pattern])})


def unsubscribe(request):
    current_user = request.user
    id = request.GET.get('id', None)
    ans = processCalendarSubscribeRequest(current_user, id)
    text = ''
    if (ans == "subscribed"):
        text = id.split('/')[-1] + " " + time.objects.filter(timeId=id)[0].lastName + " " + \
               time.objects.filter(timeId=id)[0].firstName[0] + "."
    elif (ans == "unsubscribed"):
        text = id.split('/')[-1]
    else:
        text = "Internal server error 500"
    data = {
        'answer': ans,
        'id': id,
        'text': text
    }
    thread = Thread(target=sendEmail, args=(request, id, ans,))
    thread.start()
    return JsonResponse(data)


def processCalendarSubscribeRequest(current_user, id):
    instance = time.objects.filter(timeId=id)

    if not instance.exists():
        time.objects.create(timeId=id, user=current_user.username, firstName=current_user.first_name,
                            lastName=current_user.last_name, dayId=id)
        return "subscribed"

    # Admin delete models

    if instance[0].user == current_user.username:
        instance[0].delete()
        return "unsubscribed"

    if instance[0].user != current_user.username:
        return "lackOfPermission"
