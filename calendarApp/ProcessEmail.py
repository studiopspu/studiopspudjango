import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.template.loader import get_template

def sendEmail(request, id, ans):
    if ans == "Internal server error 500":
        return
    current_user = request.user
    print(id.split('/'))
    date = id.split('/')[0] + '.' + id.split('/')[1] + '.' + id.split('/')[2]
    time = id.split('/')[3]
    user = current_user.first_name + ' ' + current_user.last_name


    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Уведомление"
    msg['From'] = 'studio@pspu.ru'
    msg['To'] = current_user.username

    text = " "
    html = get_template('email.html').render({'request': request, 'ans': ans, 'date': date, 'time': time, 'user': user})

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    msg.attach(part1)
    msg.attach(part2)

    port = 587
    smtp_server = "smtp.office365.com"
    login = "studio@pspu.ru"
    password = 'FmfF3<m_/C=j@LR'

    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo_or_helo_if_needed()
        server.starttls(context=context)
        server.ehlo_or_helo_if_needed()
        server.login(login, password)
        server.send_message(msg)
