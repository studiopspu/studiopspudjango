import locale
from calendar import Calendar
from datetime import date, datetime
from enum import Enum

from calendarApp.models import time


def buildCalendar(request, year, month):
    locale.setlocale(locale.LC_ALL, '')

    cnt = 0
    c = Calendar()
    current_user = request.user
    calendarReturn = calendarStruct()
    today = datetime.now()

    formatedTime = datetime(year, month, 1)

    calendarReturn.year = year
    calendarReturn.month = formatedTime.strftime("%B")
    calendarReturn.monthBefore = customMonth().monthBefore(year, month)
    calendarReturn.monthAfter = customMonth().monthAfter(year, month)

    for week in c.monthdatescalendar(year, month):
        weekC = weekStruct()

        for day in week:
            dayC = dayStruct()
            dayC.number = int(day.strftime("%d"))
            dayC.id = day.strftime("%d/%m/%y")

            if (year > today.year):
                dayC.noDay = False
            elif (year < today.year):
                dayC.noDay = True
            elif (year == today.year and month > today.month):
                dayC.noDay = False
            elif (year == today.year and month < today.month):
                dayC.noDay = True
            elif (dayC.number < today.day):
                dayC.noDay = True
            if (cnt == 0 and dayC.number > 20):
                dayC.noDay = True
            if (cnt > 3 and dayC.number < 20):
                dayC.noDay = True

            for i in range(9, 19):
                note = generateNote(i, dayC)
                instance = time.objects.filter(timeId=note.id)
                note = setFree(note, instance, current_user)
                note = setName(note, instance)
                dayC.time.append(note)
            weekC.isEnabled = not dayC.noDay or weekC.isEnabled
            weekC.days.append(dayC)

        cnt += 1
        calendarReturn.weeks.append(weekC)
    return calendarReturn


def generateNote(i, dayC):
    note = timeNote()
    note.isFree = 0
    note.hour = i
    note.time = str(i) + ':00'
    note.id = dayC.id + "/" + note.time
    return note


def setFree(note, instance, current_user):
    if instance.count() == 0 or not instance.exists():
        note.isFree = 1
    elif instance[0].user == current_user.username:
        note.isFree = 2
    else:
        note.isFree = 0
    return note


def setName(note, instance):
    if (note.isFree == 0 or note.isFree == 2) and len(instance[0].lastName) > 0 and len(instance[0].firstName) > 0:
        note.name = instance[0].lastName + " " + instance[0].firstName[0] + "."
    return note


class calendarStruct:

    def __init__(self):
        self.weeks = []
        self.year = ''
        self.month = ''

    weeks = []
    year = ''
    month = ''
    monthBefore = 0
    monthAfter = 0

class weekStruct:

    def __init__(self):
        self.days = []
        self.isEnabled = False

    days = []
    isEnabled = False

class customMonth:

    def __init__(self):
        self.year = 0
        self.month = 0

    year = 0
    month = 0

    def monthBefore(self, year, month):
        if (int(month) == 1):
            self.year = year - 1
            self.month = 12
            return self
        self.year = year
        self.month = month - 1
        return self

    def monthAfter(self, year, month):
        if (int(month) == 12):
            self.year = year + 1
            self.month = 1
            return self
        self.year = year
        self.month = month + 1
        return self


class dayStruct:

    def __init__(self):
        self.number = 0
        self.noDay = False
        self.id = ''
        self.time = []

    number = 0
    noDay = False
    id = ''
    time = []


# 0 - занято
# 1 - свободно
# 2 - твое

class timeNote:

    def __init__(self):
        self.isFree = 0
        self.hour = 9
        self.id = ''
        self.time = ''
        self.name = ''

    isFree = 0
    id = ''
    time = ''
    hour = 9
    name = ''

class linkType(Enum):
    pattern = 1
    div = 2
    link = 3

class HeaderNavItem:

    def __init__(self):
        self.name = ''
        self.link = ''
        self.pattern = linkType.div

    name = ''
    link = ''
    type = linkType.div

def buildHeaderNavItems(names, links, types):
    ret = []
    cnt = 0
    for itemName in names:
        HeaderNav = HeaderNavItem()
        HeaderNav.link = links[cnt]
        HeaderNav.name = itemName
        HeaderNav.type = types[cnt].value
        cnt += 1
        ret.append(HeaderNav)
    return ret
