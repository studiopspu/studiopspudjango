from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from microsoft_auth import urls

from structs import buildHeaderNavItems, linkType

login = auth_views.LoginView
login.template_name = 'registration/login.html'


urlpatterns = [
    url('login/', login.as_view(), {'headerNav': buildHeaderNavItems([1], [1], [linkType.div])}, name='login'),
    url('logout/', auth_views.LogoutView.as_view(), name='logout'),
]